function uploadFtp(done, CONFIG, IMAGES_FOLDER_PATH) {
  const FtpDeploy = require('ftp-deploy');
  const sFtpDeploy = require('node-sftp-deploy');
  const path = require('path');
  const client  = new FtpDeploy();
  const config = {
    username: CONFIG.user,
    password: CONFIG.password,
    host: CONFIG.host,
    port: CONFIG.port,
    localRoot: IMAGES_FOLDER_PATH,
    remoteRoot: CONFIG.pathToUpload
  };
  if( CONFIG.sftp) {
    console.log('deploy using protocol SFTP');
    const conFigSFTP  = {
       "host": config.host,
       "port": config.port,
       "user": config.username,
       "pass": config.password,
       "remotePath": config.remoteRoot,
       "sourcePath": config.localRoot
     };
    console.log(conFigSFTP);
    sFtpDeploy(conFigSFTP, (err) => {
      console.log(err)
      console.log('deploy completed')
    })
  } else {
    console.log('deploy using protocol FTP');
    client.deploy(config, (err) => {
      if (err) console.log(err)
      else {
        done();
      }
    })
}
}
module.exports = function(grunt) {
  grunt.registerTask('uploadftp', 'upload to Ftp Server', function() {
    const FTP_SERVER_CONFIG = grunt.config('secrets').ftp;
    const IMAGES_FOLDER_PATH = grunt.config('paths').dist_img;
    const done = this.async();
    uploadFtp(done, FTP_SERVER_CONFIG, IMAGES_FOLDER_PATH);
  })
}
